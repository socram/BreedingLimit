S4X8 BreedingLimit
==================

What is this?
-------------

This is a plugin designed for the [Bukkit]-based server [Terranova], and lets admins cap the spawning of animals to reduce server lag. Admins can select which animals will have limited its spawning based on the amount of animals in a certain radius and why it did it spawn (breeding, monster egg, ...)

Installation
------------

To install the plugin on your Bukkit server, you may either [download the pre-built version], or compile it yourself.

To compile it yourself, you need [Oracle Java Development Kit] v1.6 or newer, [Maven 2], and Git. When you have all of them downloaded and installed, you have to grab the source code using GIT and compile it:

	git clone http://github.com/socram8888/BreedingLimit
	cd BreedingLimit
	mvn clean package
	
Maven will automatically download all the dependencies. It may take more than five minutes if you have a slow internet connection, but usually it will take less than a minute.

The resulting compiled Java Archive file (.jar) ready to be used will be at /target. Just move it (or copy it) to Bukkit's plugin folder.

Configuration
-------------

The plugin configuration is quite straightforward, but here's the config.yml with some tips:

	# The folling two values specify the radius (in blocks around the entity
	# that is going to be spawned) and the max amount of entities in the
	# given radius. Entities not included in the "entities" list will be
	# ignored when counting.
	
	range: 16
	cap: 20
	
	# This specify entities classes rather than types. The good thing about
	# using classes is that you can simply put here "Animals" and every
	# animal will be added to the list of capped entities, so you don't
	# have to manually add "Cow", "Chicken", "Pig", ...
	#
	# Only living entities can be added to this list, so it won't work
	# with frames, lightning strikes...
	#
	# Please note that classes are CaSe-SeNsItIvE, so type "Pig" rather than
	# "pig".

	entities:
	 - Animals
	
	# This is the list of reasons that will be checked for population.
	# These are also CaSe-SeNsItIvE

	reasons:
	 - EGG
	 - BREEDING

For a list of all living entities see [the inheritance diagram of LivingEntity]. For an up-to-date list of spawning reasons see [the list in the Bukkit JavaDocs].

Changelog
---------
* 17/VIII/2013: 1.0
    * First public release

Usage stats
-----------

![Stats][Stats]

License
-------

This software is released under the open-source MIT license:

>Copyright © 2013 Marcos Vives Del Sol
>
>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

About the author
----------------

My name is Marcos Vives Del Sol, aka "socram8888". I'm a 18-year-old Spanish guy who likes programming useless stuff that nobody uses. If you want to report a bug, ask for a new feature, or just say hello, you can contact me in my e-mail account <socram8888@gmail.com>.

  [Bukkit]: http://www.bukkit.org/
  [Daylight Sensors]: http://www.minecraftwiki.net/wiki/Daylight_Sensor
  [download the pre-built version]: http://dev.bukkit.org/server-mods/breedinglimit/
  [Furnaces]: http://www.minecraftwiki.net/wiki/Furnace
  [Maven 2]: http://maven.apache.org/
  [Oracle Java Development Kit]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
  [Stats]: http://api.mcstats.org/signature/BreedingLimit.png
  [Terranova]: http://terranova.mine.bz/
  [the inheritance diagram of LivingEntity]: http://jd.bukkit.org/rb/doxygen/d4/d7b/interfaceorg_1_1bukkit_1_1entity_1_1LivingEntity.html
  [the list in the Bukkit JavaDocs]: http://jd.bukkit.org/rb/apidocs/org/bukkit/event/entity/CreatureSpawnEvent.SpawnReason.html
