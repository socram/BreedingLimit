
package ti.s4x8.bukkit.breedinglimit;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Logger;

import org.mcstats.MetricsLite;

public class BreedingLimit extends JavaPlugin implements Listener {
	private int cap;
	private int range;
	private final SuperClassSet entityClasses = new SuperClassSet();
	private final HashSet<CreatureSpawnEvent.SpawnReason> reasons = new HashSet<CreatureSpawnEvent.SpawnReason>();
	
	public void onEnable() {
		saveDefaultConfig();
		
		loadCap();
		loadRange();
		loadEntities();
		loadReasons();

		getServer().getPluginManager().registerEvents(this, this);
		
		try {
			(new MetricsLite(this)).start();
		} catch (IOException e) { };
	};
	
	private void loadCap() {
		cap = getConfig().getInt("cap");
		if (cap <= 0) {
			cap = 1;
		};
		getLogger().info("Cap: " + cap);
	};
	
	private void loadRange() {
		range = getConfig().getInt("range");
		if (range <= 0) {
			range = 16;
		};
		getLogger().info("Spawn range: " + range);
	};
	
	private void loadEntities() {
		Logger logger = getLogger();
		entityClasses.clear();
		Iterator<String> entityNamesIterator = getConfig().getStringList("entities").iterator();
		
		while (entityNamesIterator.hasNext()) {
			String entityName = entityNamesIterator.next();
			Class<? extends LivingEntity> entityClass;
			try {
				entityClass = (Class<? extends LivingEntity>) Class.forName("org.bukkit.entity." + entityName);
			} catch (Exception e) {
				logger.warning("Invalid entity type: " + entityName);
				continue;
			};
			
			entityClasses.add(entityClass);
		};
		
		logger.info("Entities:");
		Iterator<Class> entitiesIterator = entityClasses.iterator();
		while (entitiesIterator.hasNext()) {
			logger.info(" - " + entitiesIterator.next().getSimpleName());
		};
	};
	
	private void loadReasons() {
		Logger logger = getLogger();
		reasons.clear();
		Iterator<String> reasonNameIterator = getConfig().getStringList("reasons").iterator();
		
		while (reasonNameIterator.hasNext()) {
			String reasonName = reasonNameIterator.next();
			CreatureSpawnEvent.SpawnReason reason = null;
			try {
				reason = CreatureSpawnEvent.SpawnReason.valueOf(reasonName);
			} catch (IllegalArgumentException e) {
				logger.warning("Unknown reason: " + reasonName);
				continue;
			};
			reasons.add(reason);
		};
		
		logger.info("Reasons:");
		Iterator<CreatureSpawnEvent.SpawnReason> reasonsIterator = reasons.iterator();
		while (reasonsIterator.hasNext()) {
			logger.info(" - " + reasonsIterator.next());
		};
	};
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (!reasons.contains(event.getSpawnReason())) return;
		LivingEntity entity = event.getEntity();
		if (!entityClasses.contains(entity.getClass())) return;
		
		Iterator<Entity> it = entity.getNearbyEntities(range, range, range).iterator();
		int count = 0;
		while (it.hasNext()) {
			if (entityClasses.contains(it.next().getClass()) && ++count >= cap) {
				event.setCancelled(true);
				return;
			};
		};
	};
};
