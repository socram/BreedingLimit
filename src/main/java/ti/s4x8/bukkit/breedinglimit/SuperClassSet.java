
package ti.s4x8.bukkit.breedinglimit;

import java.util.ArrayList;
import java.util.Iterator;

/*
	This is a class that stores only parent classes.
	For example, if you have the following classes:
		- Class A0
			- Class A1 extends A0
		- Class B0
			 - Class B1 extends B0
				- Class B2 extends B1
	And you add them to a SuperClassSet, only classes "A0" and "B0" will be stored in the arraylist
	
	The "contains" method will check if the given class is related to at least one of the classes stored in the class list: either is the same class, or it extends or implements it.
*/

public class SuperClassSet {
	private final ArrayList<Class> classes = new ArrayList<Class>();

	public boolean add(Class clazz) {
		Iterator<Class> it = classes.iterator();
		while (it.hasNext()) {
			Class otherClass = it.next();
			if (otherClass.isAssignableFrom(clazz)) {
				return false;
			};
			if (clazz.isAssignableFrom(otherClass)) {
				it.remove();
			};
		};
		classes.add(clazz);
		return true;
	};

	public boolean contains(Class clazz) {
		Iterator<Class> it = classes.iterator();
		while (it.hasNext()) {
			if (it.next().isAssignableFrom(clazz)) {
				return true;
			};
		};
		return false;
	};

	public void clear() {
		classes.clear();
	};
	
	public Iterator iterator() {
		return classes.iterator();
	};
};
